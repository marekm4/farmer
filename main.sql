START TRANSACTION;
SET CONSTRAINTS ALL DEFERRED;

CREATE TABLE "chat" (
    "id" integer  NOT NULL,
    "user" integer  NOT NULL,
    "send_date" integer  NOT NULL,
    "content" varchar(200) NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "field" (
    "id" integer  NOT NULL,
    "user" integer  NOT NULL,
    "vegetable" integer  DEFAULT NULL,
    "end_date" integer  DEFAULT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "player" (
    "id" integer  NOT NULL,
    "user" integer  NOT NULL,
    "money" decimal(10,2)  NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "stock" (
    "id" integer  NOT NULL,
    "user" integer  NOT NULL,
    "vegetable" integer  NOT NULL,
    "quantity" integer  NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "user" (
    "id" integer  NOT NULL,
    "username" varchar(16) NOT NULL,
    "password" varchar(72) NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "vegetable" (
    "id" integer  NOT NULL,
    "slug" varchar(16) NOT NULL,
    "label" varchar(16) NOT NULL,
    "weather" decimal(3,2)  NOT NULL,
    "pests" decimal(3,2)  NOT NULL,
    "sell" decimal(4,2)  NOT NULL,
    "buy" decimal(4,2)  NOT NULL,
    PRIMARY KEY ("id")
);

INSERT INTO "vegetable" VALUES (1,'potato','Ziemniaki',0.25,0.25,1.00,1.20),(2,'tomato','Pomidory',0.50,0.20,1.50,1.80),(3,'cucumber','Ogórki',0.30,0.40,1.50,1.80),(4,'radish','Rzodkiewka',0.20,0.70,2.00,2.40),(5,'lettuce','Sałata',0.45,0.45,2.00,2.40),(6,'pumpkin','Dynia',0.15,0.65,2.50,3.00),(7,'clover','Koniczyna',0.70,0.10,2.50,3.00);

COMMIT;
START TRANSACTION;

-- Foreign keys --
ALTER TABLE "chat" ADD CONSTRAINT "chat_ibfk_1" FOREIGN KEY ("user") REFERENCES "user" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "chat" ("user");
ALTER TABLE "field" ADD CONSTRAINT "field_ibfk_1" FOREIGN KEY ("user") REFERENCES "user" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "field" ("user");
ALTER TABLE "field" ADD CONSTRAINT "field_ibfk_2" FOREIGN KEY ("vegetable") REFERENCES "vegetable" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "field" ("vegetable");
ALTER TABLE "player" ADD CONSTRAINT "player_ibfk_1" FOREIGN KEY ("user") REFERENCES "user" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "player" ("user");
ALTER TABLE "stock" ADD CONSTRAINT "stock_ibfk_1" FOREIGN KEY ("user") REFERENCES "user" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "stock" ("user");
ALTER TABLE "stock" ADD CONSTRAINT "stock_ibfk_2" FOREIGN KEY ("vegetable") REFERENCES "vegetable" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "stock" ("vegetable");

-- Sequences --
CREATE SEQUENCE chat_id_seq;
SELECT setval('chat_id_seq', max(id)) FROM chat;
ALTER TABLE "chat" ALTER COLUMN "id" SET DEFAULT nextval('chat_id_seq');
CREATE SEQUENCE field_id_seq;
SELECT setval('field_id_seq', max(id)) FROM field;
ALTER TABLE "field" ALTER COLUMN "id" SET DEFAULT nextval('field_id_seq');
CREATE SEQUENCE player_id_seq;
SELECT setval('player_id_seq', max(id)) FROM player;
ALTER TABLE "player" ALTER COLUMN "id" SET DEFAULT nextval('player_id_seq');
CREATE SEQUENCE stock_id_seq;
SELECT setval('stock_id_seq', max(id)) FROM stock;
ALTER TABLE "stock" ALTER COLUMN "id" SET DEFAULT nextval('stock_id_seq');
CREATE SEQUENCE user_id_seq;
SELECT setval('user_id_seq', max(id)) FROM "user";
ALTER TABLE "user" ALTER COLUMN "id" SET DEFAULT nextval('user_id_seq');
CREATE SEQUENCE vegetable_id_seq;
SELECT setval('vegetable_id_seq', max(id)) FROM vegetable;
ALTER TABLE "vegetable" ALTER COLUMN "id" SET DEFAULT nextval('vegetable_id_seq');

COMMIT;
