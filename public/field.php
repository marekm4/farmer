<?php

require_once '../functions.php';
$user = is_logged_in();

$data = get_game_data($user);
$data['title'] = 'Pole';

$data['fields'] = database_get_fields($user);
$data['price'] = calculate_field_price(count($data['fields']));

html_template('field', $data);
