<?php

require_once '../functions.php';
$user = is_logged_in();

$post = text_encode($_POST['post'], 100);
if ($_POST['csrf'] == csrf_token() && strlen($post)) {
    database_add_post($user, $post);
}

if (ajax_call()) {
    json_response([]);
} else {
    http_redirect('chat.php');
}