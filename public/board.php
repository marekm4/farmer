<?php

require_once '../functions.php';
$user = is_logged_in();

$data = get_game_data($user);
$data['title'] = 'Ogłoszenia';

$data += get_conditions();
for ($i = 0; $i < get_vegetable_count(); ++$i) {
    $id = $i + 1;
    $data['board'][$i]['id'] = $id;
    $data['board'][$i]['buy_price'] = calculate_price($id, 1);
    $data['board'][$i]['sell_price'] = calculate_price($id, 2);
    $data['board'][$i]['weather_susceptibility'] = vegetable_susceptibility($id, 1);
    $data['board'][$i]['pests_susceptibility'] = vegetable_susceptibility($id, 2);
}

html_template('board', $data);
