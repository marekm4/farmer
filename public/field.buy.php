<?php

require_once '../functions.php';
$user = is_logged_in();

$count = database_get_field_count($user);
$price = calculate_field_price($count);
$stock = database_get_stock($user);

if ($stock['money'] >= $price) {
    database_buy_field($user, $price);
}

http_redirect('field.php');
