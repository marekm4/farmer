<table>
    <tr>
        <td>Miejsce</td>
        <td>Nazwa użtykownika</td>
        <td>Pola</td>
        <td>Pieniądze</td>
    </tr>
    <?php $no = 1; ?>
    <?php foreach ($ranking as $player): ?>
        <tr>
            <td><?php echo $no++; ?></td>
            <td>
                <?php if ($player['id'] == $data['user']): ?>
                    <b><?php echo $player['username']; ?></b>
                <?php else: ?>
                    <?php echo $player['username']; ?>
                <?php endif; ?>
            </td>
            <td><?php echo $player['fields']; ?></td>
            <td><?php echo html_money($player['money']); ?></td>
        </tr>
    <?php endforeach; ?>
</table>