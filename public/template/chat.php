<div id="chat">
    <?php require_once 'template/posts.php'; ?>
</div>
<form action="chat.send.php" method="post" id="chat_form" onsubmit="return chat_send();">
    <input type="hidden" name="csrf" value="<?php echo $csrf; ?>">
    <fieldset id="post">
        <label>Wiadomość: </label>
        <input name="post" id="chat_post">
        <input type="submit" value="Wyślij">
    </fieldset>
</form>
<script src="template/chat.js"></script>