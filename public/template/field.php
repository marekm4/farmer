<?php foreach ($fields as $field): ?>
    <div class="field">
        <?php if ($field['vegetable']): ?>
            <img src="template/<?php echo html_vegetable($field['vegetable'], false); ?>.jpg">
            Gotowe o: <b><?php echo date('G:i:s', $field['end_date']); ?></b>
        <?php else: ?>
            <?php for ($i = 1; $i <= get_vegetable_count(); ++$i): ?>
                <?php $vegetable = html_vegetable($i); ?>
                <?php if ($$vegetable >= 40): ?>
                    <a href="field.sow.php?field=<?php echo $field['id']; ?>&amp;vegetable=<?php echo $i; ?>"><?php echo html_vegetable_name($i); ?></a>
                    <br>
                <?php endif; ?>
            <?php endfor; ?>
        <?php endif; ?>
    </div>
<?php endforeach; ?>
<div class="field">
    <br>
    <a href="field.buy.php">Dokup pole<br>cena: <?php echo html_money($price); ?> </a>
</div>