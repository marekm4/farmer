function market_autocomplete() {
    var market_action = $('#market_action');
    var market_stock = $('#market_stock');
    var market_value = $('#market_value');

    if (market_action.val() == 1)
        market_value.val($('#stock_' + market_stock.val()).html());
    else
        market_value.val(40);
}

function chat_scroll() {
    var chat_window = $('#chat')[0];
    $('#chat').scrollTop(chat_window.scrollHeight);
}

function chat_get() {
    var last = $('.post').last().data('id');
    $.post('chat.php', {last: last}, function (data) {
        $('#chat').append(data);
        chat_scroll();
    });
}

function chat_send() {
    var data = $('#chat_form').serialize();
    $('#chat_post').val('');
    $.post('chat.send.php', data, function (data) {
        chat_get();
    });
    return false;
}