<form action="market.trade.php" method="post">
    <fieldset id="market">
        <legend>Targ</legend>
        <label>Akcja:</label>
        <select name="action" id="market_action" onchange="market_autocomplete();">
            <option value="<?php echo SELL; ?>">Sprzedaj</option>
            <option value="<?php echo BUY; ?>">Kup</option>
        </select>
        <br>
        <label>Warzywo:</label>
        <select name="vegetable" id="market_stock" onchange="market_autocomplete();">
            <?php for ($i = 1; $i <= get_vegetable_count(); ++$i): ?>
                <option value="<?php echo $i; ?>"><?php echo html_vegetable_name($i); ?></option>
            <?php endfor; ?>
        </select>
        <br>
        <label>Ile:</label>
        <input type="text" name="count" id="market_value">
        <br>
        <label>&nbsp;</label>
        <input type="submit" value="Handluj">
    </fieldset>
</form>
<script>
    market_autocomplete();
</script>