Pogoda: <strong><?php echo html_percent($weather); ?></strong>
Szkodniki: <strong><?php echo html_percent($pests); ?></strong>
<table>
    <tr>
        <td>Nazwa</td>
        <td>Cena skupu</td>
        <td>Cena sprzedaży</td>
        <td>Wrażliwość na pogodę</td>
        <td>Wrażliwość na szkodniki</td>
    </tr>
    <?php foreach ($board as $vegetable): ?>
        <tr>
            <td><?php echo html_vegetable_name($vegetable['id']); ?></td>
            <td><?php echo html_money($vegetable['buy_price']); ?></td>
            <td><?php echo html_money($vegetable['sell_price']); ?></td>
            <td><?php echo html_percent($vegetable['weather_susceptibility']); ?></td>
            <td><?php echo html_percent($vegetable['pests_susceptibility']); ?></td>
        </tr>
    <?php endforeach; ?>
</table>