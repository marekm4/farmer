<?php if ($chat): ?>
    <?php foreach ($chat as $post): ?>
        <b><?php echo $post['username']; ?></b>
        <small>(<?php echo html_date($post['send_date']); ?>)</small>
        <span class="post" data-id="<?php echo $post['id']; ?>"><?php echo html_emoticons($post['content']); ?></span>
        <br>
    <?php endforeach; ?>
<?php endif; ?>