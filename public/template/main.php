<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $name; ?> - <?php echo $title; ?></title>
        <link rel="stylesheet" href="template/main.css" type="text/css">
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="template/main.js"></script>
    </head>
    <body>
        <div id="background">
            <div id="page">
                <div id="header">
                    <?php if (isset($user)): ?>
                        <table>
                            <tr>
                                <?php foreach ($vegetables as $vegetable): ?>
                                    <td><img src="template/<?php echo $vegetable['slug']; ?>.jpg" alt="<?php echo $vegetable['label']; ?>"></td>
                                <?php endforeach; ?>
                                <td><img src="template/money.jpg" alt="Pieniądze"></td>
                            </tr>
                            <tr>
                                <?php foreach ($vegetables as $vegetable): ?>
                                    <td id="stock_<?php echo $vegetable['id']; ?>"><?php echo ${$vegetable['slug']}; ?></td>
                                <?php endforeach; ?>
                                <td><?php echo html_money($money); ?></td>
                            </tr>
                        </table>
                    <?php else: ?>
                        <h1><?php echo $title; ?></h1>
                    <?php endif; ?>
                </div>
                <div id="nav">
                    <?php if (isset($user)): ?>
                        <a href="field.php">Pole</a>
                        <a href="board.php">Ogłoszenia</a>
                        <a href="market.php">Targ</a>
                        <a href="ranking.php">Ranking</a>
                        <a href="chat.php">Czat</a>
                        <a href="logout.php">Wyloguj się</a>
                    <?php else: ?>
                        <a href="index.php">Strona główna</a>
                        <a href="login.php">Logowanie</a>
                        <a href="register.php">Rejestracja</a>
                        <a href="terms.php">Regulamin</a>
                        <a href="contact.php">Kontakt</a>
                    <?php endif; ?>
                </div>
                <div id="main">
                    <?php if (isset($errors) && count($errors)): ?>
                        <div id="error">
                            <?php foreach ($errors as $error): ?>
                                <?php echo $error; ?>
                                <br>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php require_once __DIR__ . DIRECTORY_SEPARATOR . $view . '.php'; ?>
                </div>
            </div>
        </div>
    </body>
</html>
