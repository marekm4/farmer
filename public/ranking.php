<?php

require_once '../functions.php';
$user = is_logged_in();

$data = get_game_data($user);
$data['title'] = 'Ranking';

$data['ranking'] = database_get_ranking();

html_template('ranking', $data);
