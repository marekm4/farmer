<?php

require_once '../functions.php';

$username = text_encode($_POST['username'], 16);
$password = $_POST['password'];
$retype_password = $_POST['retype_password'];

$errors = [];

if (!$username) {
    $errors[] = 'Wpisz nazwę użtkownika.';
}

if (strlen($username) > 16) {
    $errors[] = 'Nazwa użytkownika nie może być dłuższa niż 16 znaków.';
}

if (strlen($password) < 6) {
    $errors[] = 'Hasło musi mieć co najmniej 6 znaków.';
}

if ($password != $retype_password) {
    $errors[] = 'Hasła są różne.';
}

if (database_check_username($username)) {
    $errors[] = 'Nazwa użtkownika jest już zajęta.';
}

if (!$errors) {
    $user = database_create_user($username, $password);
    login($user);
} else {
    http_redirect('register.php', ['errors' => $errors, 'username' => $username]);
}