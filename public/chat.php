<?php

require_once '../functions.php';
$user = is_logged_in();

$last = number_cast(isset($_POST['last']) ? $_POST['last'] : 0);
$chat = database_get_chat($last);

if (ajax_call()) {
    html_template('posts', array('chat' => $chat), 'ajax');
} else {
    $data = get_game_data($user);
    $data['title'] = 'Czat';
    $data['chat'] = $chat;
    html_template('chat', $data);
}