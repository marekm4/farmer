<?php

require_once '../functions.php';
$user = is_logged_in();

$action = number_cast($_POST['action'], 1, 2);
$vegetable = number_cast($_POST['vegetable'], 1, get_vegetable_count());
$count = number_cast($_POST['count']);
$stock = database_get_stock($user);

$errors = [];

if ($action == SELL) {
    if ($stock[html_vegetable($vegetable)] >= $count) {
        database_sell_vegetable($user, $vegetable, $count);
    }
} else {
    if ($stock['money'] >= (calculate_price($vegetable, $action) * $count)) {
        database_buy_vegetable($user, $vegetable, $count);
    } else {
        $errors[] = 'Nie masz tyle pieniędzy.';
    }
}

http_redirect('market.php', ['errors' => $errors]);
