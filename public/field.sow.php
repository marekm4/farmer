<?php

require_once '../functions.php';
$user = is_logged_in();

$field = number_cast($_GET['field']);
$vegetable = number_cast($_GET['vegetable'], 1, get_vegetable_count());
$stock = database_get_stock($user);

if (database_is_field_free($user, $field) && $stock[html_vegetable($vegetable)] >= 40) {
    database_sow($user, $field, $vegetable);
}

http_redirect('field.php');
