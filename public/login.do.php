<?php

require_once '../functions.php';

$username = text_encode($_POST['username'], 16);
$password = $_POST['password'];

$user = database_get_user_id($username, $password);

if ($user) {
    login($user);
} else {
    $errors[] = 'Zły login lub hasło.';
    http_redirect('login.php', ['errors' => $errors]);
}