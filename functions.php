<?php

session_start();

require_once 'library.php';
envs_load();

function get_page_data() {
    $data = array();
    $data['name'] = 'Rolnik';
    $data['csrf'] = csrf_token();
    $flash = flash_data();
    if (is_array($flash)) {
        $data += $flash;
    }

    return $data;
}

function get_game_data($user) {
    calculate_game($user);
    $data = get_page_data();
    $data['user'] = $user;
    $data += database_get_stock($user);
    $data['vegetables'] = get_vegetables();

    return $data;
}

function login($user) {
    $_SESSION['id'] = $user;
    http_redirect('field.php');
}

function is_logged_in() {
    if ($user = $_SESSION['id']) {
        return $user;
    } else {
        http_redirect('login.php');
    }
}

define('SELL', 1);
define('BUY', 2);

function calculate_game($user) {
    $dane = database_table('select vegetable, id from field where vegetable is not null and "user"=' . $user . ' and end_date<=' . time()); //znajdź gotowe

    for ($i = 0; $i < count($dane); ++$i) {
        $pole = $dane[$i];
        $vegetable = $pole['vegetable'];
        $crop = calculate_crop($pole['vegetable']);
        database_query("update stock set quantity = quantity + $crop where vegetable = $vegetable and \"user\" = $user"); //dodaj surowce
        database_query('update field set vegetable = null, end_date = null where id=' . $pole['id']); //wyczyść pole
    }
}

function vegetable_susceptibility($co, $warunek) { //$warunek podadność na który warunek zwrócić
    $podatnosc = array_map(function ($vegetable) {
        return [$vegetable['weather'], $vegetable['pests']];
    }, get_vegetables());

    return $podatnosc[$co - 1][$warunek - 1]; //numerujemy od jedynki, warunki też od jedynki
}

function calculate_price($co, $akcja) { //$akcja sprzedaj-1, kup-2
    $warunki = get_conditions();
    $pogoda = $warunki['weather'];
    $szkodniki = $warunki['pests'];
    $pogoda = 1 - $pogoda; //zamiana pogody z pozytywnej na negatywną
    $mnoznik = 1 + ($pogoda + $szkodniki) / 2;
    $cena = array_map(function ($vegetable) {
        return [$vegetable['sell'], $vegetable['buy']];
    }, get_vegetables());

    return round($cena[$co - 1][$akcja - 1] * $mnoznik, 2); //numerujemy od jedynki, warunki też od jedynki
}

function calculate_field_price($field_count) {
    return pow($field_count, 2) * 100;
}

function get_vegetable_count($filter = true) {
    return count(get_vegetables($filter));
}

function get_cached_vegetables() {
    static $vegetables = null;
    if (is_null($vegetables)) {
        if (!$vegetables = cache_fetch('vegetables')) {
            $vegetables = database_table('select * from vegetable');
            cache_store('vegetables', $vegetables);
        }
    }

    return $vegetables;
}

function get_vegetables($filter = true) {
    $vegetables = get_cached_vegetables();

    if ($filter) {
        $vegetables = array_filter($vegetables, 'vegetables_filter');
    }

    return $vegetables;
}

function vegetables_filter($vegetable) {
    $filters['pumpkin'] = ['month' => 10, 'day' => 31];
    $filters['clover'] = ['month' => 3, 'day' => 17];

    $slug = $vegetable['slug'];

    if (array_key_exists($slug, $filters)) {
        $now = time();
        $day = mktime(0, 0, 0, $filters[$slug]['month'], $filters[$slug]['day'], date('Y'));
        $start = $day - (86400 * 7);
        $end = $day + (86400 * 2);

        return $start <= $now && $now <= $end;
    }

    return true;
}

function get_conditions() {
    $day = date('z');

    return array('weather' => warunki($day, 23), 'pests' => warunki($day, 29));
}

function warunki($dzien, $cykl) {
    return round(0.5 + (0.5 * sin((($dzien % $cykl) / $cykl) * 2 * pi())), 2);
}

function calculate_crop($co) { //funcja oblicza ile zwrócić na podstawie warunków
    $warunki = get_conditions();
    $pogoda = $warunki['weather'];
    $szkodniki = $warunki['pests'];
    $pogoda = 1 - $pogoda; //zamiana pogody z pozytywnej na negatywną
    $pogoda_strata = vegetable_susceptibility($co, 1) * $pogoda; //straty spowodowane pogodą
    $drugi_strata = vegetable_susceptibility($co, 2) * $szkodniki; //straty spowodowane drugim czynnikiem
    $plon = 130 - (100 * $pogoda_strata) - (100 * $drugi_strata) + rand(0, 5); //główny plon
    return $plon;
}

function database_get_user_id($username, $password) {
    $user = database_row("select id, password from \"user\" where username='$username'");
    if (password_verify($password, $user['password'])) {
        return $user['id'];
    }
}

function database_check_username($username) {
    return database_row("select id from \"user\" where username='$username'");
}

function database_create_user($username, $password) {
    $password = password_hash($password, PASSWORD_BCRYPT);
    $user = database_scalar("insert into \"user\"(username, password) values('$username', '$password') returning id");
    database_query('insert into player("user", money) values(' . $user . ', 0)');
    for ($i = 1, $start = 50; $i <= get_vegetable_count(false); ++$i) {
        database_query("insert into stock(\"user\", vegetable, quantity) values($user, $i, $start)");
        $start = 0;
    }
    database_query('insert into field("user", vegetable, end_date) values(' . $user . ', null, null)');

    return $user;
}

function database_get_stock($user) {
    $vegetables = database_table("select slug, quantity from stock join vegetable on stock.vegetable = vegetable.id where stock.user = $user");
    foreach ($vegetables as $vegetable) {
        $stock[$vegetable['slug']] = $vegetable['quantity'];
    }
    $stock['money'] = database_scalar("select money from player where \"user\"=$user");

    return $stock;
}

function database_get_fields($user) {
    return database_table('select * from field where "user"=' . $user . ' order by id');
}

function database_get_field_count($user) {
    return database_scalar('select count(*) from field where "user"=' . $user);
}

function database_buy_field($user, $price) {
    database_sub_money($user, $price);
    database_query('insert into field("user", vegetable, end_date) values(' . $user . ', null, null)');
}

function database_is_field_free($user, $field) {
    return database_row('select id from field where vegetable is null and id=' . $field . ' and "user"=' . $user);
}

function database_sow($user, $field, $vegetable) {
    database_query("update stock set quantity = quantity - 40 where vegetable = $vegetable and \"user\" = $user");
    database_query('update field set vegetable=' . $vegetable . ', end_date=' . (time() + 6 * 60 * 60) . ' where id=' . $field);
}

function database_sell_vegetable($user, $vegetable, $count) {
    database_add_money($user, calculate_price($vegetable, SELL) * $count);
    database_query("update stock set quantity = quantity - $count where vegetable = $vegetable and \"user\" = $user");
}

function database_buy_vegetable($user, $vegetable, $count) {
    database_sub_money($user, calculate_price($vegetable, BUY) * $count);
    database_query("update stock set quantity = quantity + $count where vegetable = $vegetable and \"user\" = $user");
}

function database_add_money($user, $amount) {
    database_query("update player set money=money+($amount) where \"user\"=$user");
}

function database_sub_money($user, $amount) {
    database_add_money($user, -$amount);
}

function database_add_post($user, $post) {
    $time = time();
    database_query("insert into chat(\"user\", send_date, content) values($user, $time, '$post')");
}

function database_get_chat($last = 0) {
    return array_reverse(database_table("select chat.id, username, send_date, content from chat join \"user\" on chat.user=\"user\".id where chat.id > $last order by id desc limit 100"));
}

function database_get_ranking() {
    return database_table('select "user".id, username, (select count(*) from field where field.user = "user".id) as fields, (select money from player where player.user = "user".id) as money from "user" order by fields desc, money desc');
}

function html_emoticons($text) {
    $smiles = array('!!', '??', ':D', ':P', ':)', ':(');
    $images = array('exclamation', 'question', 'smile', 'tongue', 'happy', 'sad');
    for ($i = 0; $i < count($images); ++$i) {
        $images[$i] = "<img src=\"template/$images[$i].gif\" alt=\"$smiles[$i]\" class=\"emoticon\">";
    }

    return nl2br(str_ireplace($smiles, $images, $text));
}

function html_date($date) {
    $days_absolute = array('Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela');
    $days_relative = array('Przedwczoraj', 'Wczoraj', 'Dzisiaj', 'Jutro', 'Pojutrze');
    if (date('Y') == date('Y', $date)) {
        $diff = date('z', $date) - date('z');
        if (abs($diff) <= 2) {
            $day = $days_relative[$diff + 2];
        } elseif (abs($diff) <= 7) {
            $day = $days_absolute[date('N', $date) - 1];
        } else {
            $day = date('d.m', $date);
        }
    } else {
        $day = date('d.m.Y', $date);
    }

    return $day . ' ' . date('G:i', $date);
}

function html_vegetable($co, $filter = true) {
    $dane = array_column(get_vegetables($filter), 'slug');

    return $dane[$co - 1];
}

function html_vegetable_name($co) {
    $dane = array_column(get_vegetables(), 'label');

    return $dane[$co - 1];
}

function html_money($value) {
    return number_format($value, 2, ',', '&nbsp;') . 'zł';
}

function html_percent($value) {
    return ($value * 100) . '%';
}
